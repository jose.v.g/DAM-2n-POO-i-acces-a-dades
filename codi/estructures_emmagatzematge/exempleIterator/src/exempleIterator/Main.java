package exempleIterator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		String[] cadenes = {"a", "b", "c", "d", "b", "d", "a", "c", "b", "c"};
		List<String> llista1 = new ArrayList<>(Arrays.asList(cadenes));
		List<String> llista2 = new LinkedList<>(Arrays.asList(cadenes));
		
		eliminaElement(llista1, "b");
		eliminaElement(llista2, "d");
		mostraLlista(llista1);
		System.out.println();
		mostraLlista(llista2);
	}
	
	public static void eliminaElement(List<String> llista, String aEliminar) {
		Iterator<String> it = llista.iterator();
		
		while (it.hasNext()) {
			if (it.next().equals(aEliminar))
				it.remove();
		}
	}
	
	public static void mostraLlista(List<String> llista) {
		for (String s : llista) {
			System.out.println(s);
		}
	}

}
