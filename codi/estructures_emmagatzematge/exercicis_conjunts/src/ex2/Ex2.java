package ex2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class Ex2 {

	public static void main(String[] args) {
		Set<Integer> divisorsA = new HashSet<Integer>();
		Set<Integer> divisorsB = new HashSet<Integer>();
		Set<Integer> divisorsAiB;
		Set<Integer> divisorsAoB;
		Set<Integer> divisorsNiANiB;
		List<Integer> llista;
		int n, a, b;
		Scanner scanner = new Scanner(System.in);

		try {
			// Demanem dos nombres enters
			System.out.println("Primer nombre: ");
			a = scanner.nextInt();
			scanner.nextLine();
			System.out.println("Segon nombre: ");
			b = scanner.nextInt();
			scanner.nextLine();
			// Omplim dos conjunts amb els divisors d'a i els de b
			for (n = 2; n < 1001; n++) {
				if (a % n == 0)
					divisorsA.add(n);
				if (b % n == 0)
					divisorsB.add(n);
			}
			// Utilitzem operacions de conjunts per trobar:
			// 1- Divisors comuns
			divisorsAiB = new HashSet<Integer>(divisorsA);
			divisorsAiB.retainAll(divisorsB);
			// 2- Divisors d'un dels dos
			divisorsAoB = new HashSet<Integer>(divisorsA);
			divisorsAoB.addAll(divisorsB);
			// 3- No divisors d'a ni de b
			divisorsNiANiB = new HashSet<Integer>();
			for (n = 2; n < 101; n++)
				divisorsNiANiB.add(n);
			divisorsNiANiB.removeAll(divisorsAoB);

			// Mostrem els resultats ordenats
			System.out.println("Divisors de " + a + " i " + b + ": ");
			llista = new ArrayList<Integer>(divisorsAiB);
			Collections.sort(llista);
			System.out.println(llista);
			System.out.println("Divisors de " + a + " o " + b + ": ");
			llista = new ArrayList<Integer>(divisorsAoB);
			Collections.sort(llista);
			System.out.println(llista);
			System.out.println("No són divisors de " + a + " ni de " + b + ": ");
			llista = new ArrayList<Integer>(divisorsNiANiB);
			Collections.sort(llista);
			System.out.println(llista);
		} catch (InputMismatchException e) {
			System.err.println("Es necessita un nombre enter");
		}
		scanner.close();
	}

}
