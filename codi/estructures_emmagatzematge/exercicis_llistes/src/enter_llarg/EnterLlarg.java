package enter_llarg;

import java.util.ArrayList;
import java.util.ListIterator;

public class EnterLlarg {
	private boolean negatiu = false;
	private ArrayList<Byte> magnitud = new ArrayList<Byte>();
	
	private void estandaritza() {
		while (magnitud.size() > 0 && magnitud.get(0) == 0)
			magnitud.remove(0);
		
		if (magnitud.size() == 0)
			negatiu = false;
	}
	
	public EnterLlarg() {
		estandaritza();
	}
	
	public EnterLlarg(long n) {
		negatiu = n < 0;
		n = Math.abs(n);
		while (n > 0) {
			magnitud.add(0, (byte)(n % 10));
			n /= 10;
		}
		estandaritza();
	}
	
	public EnterLlarg(byte[] xifres) throws NumberFormatException {
		for (byte xifra : xifres) {
			if (xifra < 0 || xifra > 9)
				throw new NumberFormatException("Cada xifra ha d'estar entre 0 i 9");
			magnitud.add(xifra);
		}
		estandaritza();
	}
	
	public EnterLlarg(String representacio) throws NumberFormatException {
		int i;
		Byte b;
		
		if (representacio.charAt(0) == '-') {
			representacio = representacio.substring(1);
			negatiu = true;
		}
		
		for (i=0; i<representacio.length(); i++) {
			b = new Byte(representacio.substring(i, i+1));
			magnitud.add(b);
		}
		estandaritza();
	}
	
	@Override
	public String toString() {
		String representacio = "";
		
		if (negatiu)
			representacio = "-";
		
		if (magnitud.size() > 0) {
			for (Byte xifra : magnitud)
				representacio += xifra;
		} else
			representacio += "0";
		
		return representacio;
	}
	
	public EnterLlarg suma(EnterLlarg n) throws UnsupportedOperationException {
		int parcial;
		int menporto = 0;
		Byte xifraA, xifraB;
		EnterLlarg resultat = new EnterLlarg();
		ListIterator<Byte> itA = magnitud.listIterator(magnitud.size());
		ListIterator<Byte> itB = n.magnitud.listIterator(n.magnitud.size());
		
		if (negatiu != n.negatiu)
			throw new UnsupportedOperationException("La resta no està implementada.");
		else
			resultat.negatiu = negatiu;

		while (itA.hasPrevious() || itB.hasPrevious()) {
			xifraA = itA.hasPrevious() ? itA.previous() : 0;
			xifraB = itB.hasPrevious() ? itB.previous() : 0;
			parcial = xifraA + xifraB + menporto;
			if (parcial > 9) {
				parcial-=10;
				menporto = 1;
			} else
				menporto = 0;
			resultat.magnitud.add(0,(byte)parcial);
		}
		if (menporto > 0)
			resultat.magnitud.add(0,(byte)menporto);
		return resultat;
	}
}
