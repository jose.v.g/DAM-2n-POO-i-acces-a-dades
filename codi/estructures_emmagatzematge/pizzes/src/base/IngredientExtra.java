package base;

public abstract class IngredientExtra implements Ingredient, Cloneable {
	private final Ingredient ingredient;
	
	public IngredientExtra(Ingredient base) {
		ingredient = base;
	}
	
	@Override
	public String recepta() {
		return ingredient.recepta();
	}
	
	@Override
	public String descripcio() {
		return ingredient.descripcio();
	}
	
	@Override
	public double getPreu() {
		return ingredient.getPreu();
	}
	
	@Override
	public IngredientExtra clone() throws CloneNotSupportedException {
		return (IngredientExtra) super.clone();
	}
}
