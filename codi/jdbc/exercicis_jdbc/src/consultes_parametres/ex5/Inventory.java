package consultes_parametres.ex5;

import java.util.Date;

public class Inventory {
	public final int id;
	public final String title;
	public final Date dueDate;
	
	public Inventory(int id, String title, Date dueDate) {
		this.id = id;
		this.title = title;
		this.dueDate = dueDate;
	}
}
