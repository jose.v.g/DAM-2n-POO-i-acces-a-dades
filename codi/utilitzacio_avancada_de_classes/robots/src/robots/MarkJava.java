package robots;
import java.util.Random;

public abstract class MarkJava {
	protected static Random random = new Random();
	private int energia = 10;
	public boolean haMogut = false;
	
	public boolean decideixSiMou() {
		if (haMogut == true)
			return false;
		haMogut = true;
		return true;
	}
	
	public abstract void interactua(MarkJava unAltreRobot);
	
	public void gastaEnergia(int energiaGastada) throws IllegalArgumentException {
		energia -= energiaGastada;
		if (energia < 0)
			throw new IllegalArgumentException("Energia negativa");
	}
	
	public void gastaBateria() {
		energia = 0;
	}
	
	public void recarregaBateria() {
		energia = 10;
	}
	
	public int obteEnergia() {
		return energia;
	}
}
