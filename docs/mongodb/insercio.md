## Inserció de documents

En el següent exemple podem veure com inserir un document a una col·lecció
de MongoDB des de Java. Primer buidem la col·lecció per assegurar que només
tenim aquest document.

```java
public class ExempleInsert {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("exemples");
		MongoCollection<Document> coll = db.getCollection("mascotes");

		coll.drop();

		Document document = new Document("nom", "Buffy")
				.append("edat", 3)
				.append("espècie", "gat");
		System.out.println(document.toJson());
		coll.insertOne(document);
		System.out.println(document.toJson());
		client.close();
	}
}
```

La sortida del programa és:

```
{ "nom" : "Buffy", "edat" : 3, "espècie" : "gat" }
{ "_id" : { "$oid" : "56ab8525ec24140abcbe496a" }, "nom" : "Buffy", "edat" : 3, "espècie" : "gat" }
```

Podem veure que el document no és exactament igual abans d'inserir-lo a la
col·lecció que després: després té un camp nou que no
havíem especificat en el codi Java.

Això també ho podem comprovar si anem a la consola de MongoDB i mirem què
ha passat:

```
> show databases
exemples  0.000GB
local     0.000GB
> use exemples
switched to db exemples
> show collections
mascotes
> db.mascotes.find()
{ "_id" : ObjectId("5695548fec24140c88a63bf1"), "nom" : "Buffy", "edat" : 3, "espècie" : "gat" }
>
```

Aquest camp és l'identificador únic de l'objecte. Hauríem pogut afegir-lo
nosaltres com un camp més quan hem creat el document
en Java, però si no ho fem, el MongoDB ens en genera un automàticament el
primer cop que inserim el document.
