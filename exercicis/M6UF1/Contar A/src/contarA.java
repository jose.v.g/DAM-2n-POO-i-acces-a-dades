import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class contarA {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String nomFitxer;
        int lectura, cont =0;
        char ch;
        
        System.out.println("Introdueix la direccio del fitxer a llegir: ");
        nomFitxer = scan.nextLine();
        
        try (FileReader lector = new FileReader(nomFitxer)) {
            while ((lectura = lector.read())!=-1) {
                ch = (char) lectura;
                if (ch == 'a' || ch == 'A') {
                    cont++;
                }
            }
            System.out.println("En aquest fitxer hi han "+cont+" lletres 'a'.");
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
		
	}
}
