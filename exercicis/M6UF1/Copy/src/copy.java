import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class copy {

	public static void main(String[] args) throws IOException {
		File origen = new File(args[0]);
		File destino = new File(args[1]);
		
		InputStream in = new FileInputStream(origen);
		OutputStream out = new FileOutputStream(destino);

		byte[] buf = new byte[1024];
		int len;
		
		while((len = in.read(buf))>0){
			out.write(buf, 0, len);
		}

		in.close();
		out.close();
	}
}
