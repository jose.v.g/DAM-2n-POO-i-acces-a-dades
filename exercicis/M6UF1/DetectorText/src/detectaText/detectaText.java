package detectaText;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class detectaText {
	public static void main(String[] args) {
		
        char text;
        
		for (int i = 0; i < args.length; i++) {
			System.out.println("Text del fitxer "+i);
			try (DataInputStream lector = new DataInputStream(new FileInputStream(args[i]))) {
				text = lector.readChar();
		        System.out.format("%c", text);
		    } catch (FileNotFoundException ex) {
		        System.err.println(ex);
		    } catch (IOException ex) {
		        System.err.println(ex);
		    }
			System.out.println("");
		}
	}
}
