package pkg1;

import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Deque;
import java.util.LinkedList;

public class ClaselsRecursivo {
	
	public static void main(String[] args) {
			
		Deque<Path> pila = new LinkedList<Path>();
			
        Path dir = Paths.get(args[0]);
        
        String permis = new String();                
        
    	pila.push(dir);
    	
    	while (pila.size() > 0) {
    		System.out.println("\nFitxers del directori:\t" + pila.peek());
    		
    		try (DirectoryStream<Path> stream = Files.newDirectoryStream(pila.pop())) {
            	
                for (Path fitxer: stream) {
                	
                	if (Files.isDirectory(fitxer)){
                			pila.push(fitxer);
                	}
                	
                	if (Files.isDirectory(fitxer)){
        	    		permis += "d";
        	    	}else{
        	    		permis += "-";
        	    	}
        	    	
        	    	if (Files.isReadable(fitxer)){
        	    		permis += "r";
        	    	}else{
        	    		permis += "-";
        	    	}
        	    	
        	    	if (Files.isWritable(fitxer)){
        	    		permis += "w";
        	    	}else{
        	    		permis += "-";
        	    	}
        	    	
        	    	if (Files.isExecutable(fitxer)){
        	    		permis += "x";
        	    	}else{
        	    		permis += "-";
        	    	}
                    System.out.println(permis + " " + fitxer.getFileName());
                    permis = "";
                    
                }
            } catch (IOException | DirectoryIteratorException ex) {   
            
                System.err.println(ex);
            }
	    }
	}
}
