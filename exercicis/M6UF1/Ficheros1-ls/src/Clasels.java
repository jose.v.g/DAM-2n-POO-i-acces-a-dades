import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

//Fes un programa que rebi un nom de directori com a 
//par�metre i mostri el seu contingut, indicant en cada 
//cas si es tracta d'un fitxer o directori i els permisos 
//que tenim sobre ell.

//La sortida tindr� un aspecte similar a aquest:
//-rw- fitxer
//drwx directori

public class Clasels {

	public static void main(String[] args) {        
        if (args.length == 1) {
            Path dir = Paths.get(args[0]);
            String permis = "";
            System.out.println("Fitxers del directori " + dir);
            if (Files.isDirectory(dir)) {
                try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
                    for (Path fitxer: stream) {
                    	if (Files.isDirectory(fitxer)){
            	    		permis += "d";
            	    	}else{
            	    		permis += "-";
            	    	}
            	    	
            	    	if (Files.isReadable(fitxer)){
            	    		permis += "r";
            	    	}else{
            	    		permis += "-";
            	    	}
            	    	
            	    	if (Files.isWritable(fitxer)){
            	    		permis += "w";
            	    	}else{
            	    		permis += "-";
            	    	}
            	    	
            	    	if (Files.isExecutable(fitxer)){
            	    		permis += "x";
            	    	}else{
            	    		permis += "-";
            	    	}
                        System.out.println(permis + " " + fitxer.getFileName());
                        permis = "";
                    }
                } catch (IOException | DirectoryIteratorException ex) {         
                    System.err.println(ex);
                }
            } else {
                System.err.println(dir.toString()+" no �s un directori");
            }
        } else {
            System.err.println("�s: java LlistarDirectori <directori>");
        }
    }
}
