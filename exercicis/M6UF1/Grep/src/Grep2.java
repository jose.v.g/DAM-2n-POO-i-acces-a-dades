import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Grep2 {

	public static void main(String[] args) {
		
		boolean correcte;
		int i;
		Path path;

		if (args.length >= 2) {
			path = Paths.get(args[1]);
			if (Files.isDirectory(path) && args.length == 2) {
				try (DirectoryStream<Path> stream = Files.newDirectoryStream(path)) {
					for (Path file: stream) {
						if (Files.isRegularFile(file))
							grep(args[0], file);
					}
				} catch (IOException | DirectoryIteratorException ex) {		    
					System.err.println(ex);
				}
			} else {
				correcte = true;
				i = 1;
				while (i < args.length && correcte) {
					path = Paths.get(args[i]);
					if (!Files.isRegularFile(path))
						correcte = false;
					i++;
				}
				if (correcte) {
					for (i = 1; i < args.length; i++) {
						path = Paths.get(args[i]);
						grep(args[0], path);
					}
				} else {
					System.err.println("Error de sintaxis.");
				}
			}
		} else {
			System.err.println("Error de sintaxis.");
			System.err.println("Sintaxi per a directoris: GrepNoTanSimple cadena directori");
			System.err.println("Sintaxi per a fitxers: GrepNoTanSimple cadena fitxer...");
		}
	}
	
	public static void grep(String buscar, Path direc){
		String linia;
		try(BufferedReader reader = new BufferedReader(new FileReader(direc.toString()))){
			while ((linia = reader.readLine()) != null){
				if(linia.contains(buscar)){
					System.out.println(linia);
				}
			}
		}catch(FileNotFoundException e) {
			System.out.println("Error en obrir el fitxer. --> "+e.getMessage());
		}catch(IOException e) {
			System.out.println("Error de lectura. --> "+e.getMessage());
		}
	}
}
