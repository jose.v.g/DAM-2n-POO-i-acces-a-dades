import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class GrepSimple {

	public static void main(String[] args) {
		if(args.length == 2){
			String buscar = args[0];
			String direc = args[1];
			
			Path path = Paths.get(direc);
			if(Files.isRegularFile(path) && Files.isReadable(path)){
				grep(buscar, direc);
			}else {
				System.out.println("No es pot accedir a: " + args[1]);
			}
		}else{
			System.out.println("Cuantitat de parametres pasats incorrecte.");
		}
	}
	
	public static void grep(String buscar, String direc){
		String linia;
		try(BufferedReader reader = new BufferedReader(new FileReader(direc))){
			while ((linia = reader.readLine()) != null){
				if(linia.contains(buscar)){
					System.out.println(linia);
				}
			}
		}catch(FileNotFoundException e) {
			System.out.println("Error en obrir el fitxer. --> "+e.getMessage());
		}catch(IOException e) {
			System.out.println("Error de lectura. --> "+e.getMessage());
		}
	}
}
