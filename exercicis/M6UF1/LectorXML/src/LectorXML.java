import java.io.IOException;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class LectorXML {

	public static void main(String[] args) {
		try {
			XMLReader procesXML = XMLReaderFactory.createXMLReader();
			procesXML.setContentHandler(new Controlador());
			procesXML.parse(new InputSource("mascotes.xml"));
		} catch (SAXException | IOException e) {
			System.err.println(e.getMessage());
		}
	}
}
