import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class MiniShell {
	
	static Path dire = Paths.get(System.getProperty("user.dir"));

	public static void main(String[] args) {
		String cmd = "";
		String[] sep_cmd;
		Scanner scan = new Scanner(System.in);
		
		while (!cmd.equals("exit")) {
			System.out.print(dire.normalize() + "> ");
			cmd = scan.nextLine();
			sep_cmd = cmd.split("\\s", 2);
			
				switch (sep_cmd[0]) {
				case "ls":
					try (DirectoryStream<Path> stream = Files.newDirectoryStream(dire)) {
						for (Path fitxer : stream) {
							System.out.println("  " + fitxer.getFileName() + (Files.isDirectory(fitxer) ? "/" : ""));
						}
					} catch (IOException | DirectoryIteratorException ex) {
						System.err.println(ex);
					}
					System.out.println();
				break;
	
				case "cd":
					if (!(sep_cmd.length <2)) {
						Path path = dire.resolve(sep_cmd[1]);
						if (Files.isDirectory(path)){
							dire = path.toAbsolutePath().normalize();
						}else{
							System.err.println("No es troba el directori");
						}
					}else{
						System.out.println("Falten dades per operar.");
					}
				break;
	
				case "cat":
					if (!(sep_cmd.length <2)) {
						String s;
						Path path = dire.resolve(sep_cmd[1]);
						try (BufferedReader lector = new BufferedReader(new FileReader(path.toString()))) {
							while ((s = lector.readLine()) != null)
								System.out.println(s);
							System.out.println();
						} catch (FileNotFoundException e) {
							System.err.println("No s'ha trobat el fitxer");
						} catch (IOException e) {
							System.err.println("No es pot llegir el fitxer: " + e.getMessage());
						}
					}else{
						System.out.println("Falten dades per operar.");
					}
				break;
	
				default:
					System.out.println("Comanda no reconeguda.");
					break;
			}
		}
		scan.close();
	}
}
