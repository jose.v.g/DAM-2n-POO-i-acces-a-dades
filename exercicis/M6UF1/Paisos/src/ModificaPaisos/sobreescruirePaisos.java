package ModificaPaisos;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class sobreescruirePaisos {
	private static String readChars(RandomAccessFile fitxer, int nChars) throws IOException {
        StringBuilder b = new StringBuilder();
        char ch = ' ';
        for (int i=0; i<nChars; i++) {
            ch=fitxer.readChar();
            if (ch != '\0')
                b.append(ch);
        }
        return b.toString();
    }
	
	public static void main(String[] args) {
        String nom;
        String opcio;
        String novaDada = new String();
        int id, poblacio;
        long pos=0;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Introdueix n�mero de registre a modificar: ");
        id=scanner.nextInt();
        scanner.nextLine();
        try (RandomAccessFile fitxer = new RandomAccessFile("paisos.dat", "rw")) {
            // per accedir a un registre multipliquem per la mida de
            // cada registre.
        	pos=(id-1)*174;
        	
        	if (pos<0 || pos>=fitxer.length())
                throw new IOException("N�mero de registre inv�lid.");
        	
        	System.out.println("Que vols modificar? ");
        	opcio = scanner.nextLine();
        	opcio = opcio.toLowerCase();
        	
        	//id  ------> int  (4 bytes)
        	//nom ------> char (2 bytes) * 40 car�cters
        	//Codi ISO -> char (2 bytes) * 3 car�cters
        	//Capital --> char (2 bytes) * 40 car�cters
        	//poblaci� -> int  (4 bytes)
        	
        	
			fitxer.seek(pos);
            fitxer.readInt();
            nom = readChars(fitxer, 0);
            
			System.out.println("Introdueix la nou nom: ");
            novaDada = scanner.nextLine();
            
            if (novaDada.length() >= 0) {
                pos = fitxer.getFilePointer()-40;
                fitxer.seek(pos);
                fitxer.writeBytes(novaDada);
            } else {
                System.err.println("El nom no pot estar buit.");
            }

			fitxer.seek(pos);
            fitxer.readInt();
            nom = readChars(fitxer, 40);
            
			System.out.println("Introdueix la nou codiISO: ");
            novaDada = scanner.nextLine();
            
            if (novaDada.length() >= 0) {
                pos = fitxer.getFilePointer()-3;
                fitxer.seek(pos);
                fitxer.writeBytes(novaDada);
            } else {
                System.err.println("El codiISO no pot estar buit.");
            }
            
			fitxer.seek(pos);
            fitxer.readInt();
            nom = readChars(fitxer, 43);
            
			System.out.println("Introdueix la nova capital: ");
            novaDada = scanner.nextLine();
            
            if (novaDada.length() >= 0) {
                pos = fitxer.getFilePointer()-40;
                fitxer.seek(pos);
                fitxer.writeBytes(novaDada);
            } else {
                System.err.println("La capital no pot estar buida.");
            }
            
			fitxer.seek(pos);
            fitxer.readInt();
            nom = readChars(fitxer, 40);
            readChars(fitxer, 43);
            poblacio = fitxer.readInt();
            
			System.out.println("Introdueix la nova poblaci�: ");
            poblacio = scanner.nextInt();
            scanner.nextLine();
            
            if (poblacio >= 0) {
                pos = fitxer.getFilePointer()-4;
                fitxer.seek(pos);
                fitxer.writeInt(poblacio);
            } else {
                System.err.println("La poblaci� ha de ser positiva.");
            }
			
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        scanner.close();
    }
}
