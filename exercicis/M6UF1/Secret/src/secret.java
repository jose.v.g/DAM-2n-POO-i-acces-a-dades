import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

public class secret {

	public static void main(String[] args) {

		Random rand = new Random();
		char[] letras = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
		int num = 0;
		int i = 0;

        try (DataOutputStream escriptor = new DataOutputStream(new FileOutputStream(args[0]))) {
        	while(i<1000){
        		num += rand.nextInt(3);
        		escriptor.writeInt(num);
        		
        		escriptor.writeChar(letras[rand.nextInt(letras.length)]);
        		escriptor.writeChar(letras[rand.nextInt(letras.length)]);
        		escriptor.writeChar(letras[rand.nextInt(letras.length)]);
        		i++;
        	}
            
        } catch (FileNotFoundException ex) {
            System.err.println(ex);
        } catch (IOException ex) {
            System.err.println(ex);
        }
	}
	
}
