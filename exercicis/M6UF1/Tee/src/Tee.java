import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public class Tee {

	public static void main(String[] args) {
		int n;
		char c;
		if (args.length == 1) {
			try (InputStreamReader lector = new InputStreamReader(System.in);
					FileWriter escriptor = new FileWriter(args[0])) {
				while (((char)(n = lector.read())) != '.') {
					c = (char) n;
					System.out.print(c);
					escriptor.write(n);
				}
				System.out.println("Adios");
			} catch (IOException e) {
				System.err.println("Error d'escriptura: " + e.getMessage());
			}
		} else {
			System.err.println("Sintaxi: <<Nom del fitxer>>");
		}
	}
}
