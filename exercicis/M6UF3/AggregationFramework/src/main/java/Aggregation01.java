import java.util.Arrays;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;

public class Aggregation01 {

	public static void main(String[] args) {
		MongoClient cli = new MongoClient();
		MongoDatabase db = cli.getDatabase("Practiques");
		MongoCollection<Document> coll = db.getCollection("grades");
		
		coll.aggregate(Arrays.asList(
			Aggregates.unwind("$scores"),
			Aggregates.match(Filters.eq("scores.type","exam")),
			Aggregates.group("$name", Accumulators.avg("nota", "$scores.score")),
			Aggregates.match(Filters.gte("nota", 40))
		)).forEach((Document doc) -> {
			System.out.println(doc.getString("_id")+" ("+doc.getDouble("nota")+")");
		});
		cli.close();
	}
}
