import java.util.Arrays;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Sorts;

public class Aggregation02 {
	private static String lastId = "";

	public static void main(String[] args) {
		MongoClient cli = new MongoClient();
		MongoDatabase db = cli.getDatabase("Practiques");
		MongoCollection<Document> coll = db.getCollection("grades");
		
		coll.aggregate(Arrays.asList(
				//Aggregates.lookup("stdents", "_id", "student_id", "stdnt_data"),
				Aggregates.unwind("$scores"),
				Aggregates.group(new Document("id","$_id").append("name", "$name").append("type", "$scores.type"), Accumulators.sum("quantitat", 1)),
				Aggregates.sort(Sorts.ascending("_id.name", "_id.id"))
			)).forEach((Document doc) -> {
				//System.out.println(doc);
				Document id = doc.get("_id", Document.class);
				if (!lastId.equals(id.get("id").toString())) {
					lastId = id.get("id").toString();
					System.out.println("Alumne "+id.get("id").toString()+":");
				}
				System.out.println("  Tipus: "+id.getString("type")+",  Quantitat: "+doc.getInteger("quantitat"));
			});
		cli.close();
	}
}
