import java.util.Arrays;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;

public class Aggregation03 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("Practiques");
		MongoCollection<Document> coll = db.getCollection("stdents");

		coll.aggregate(Arrays.asList(
			Aggregates.unwind("$scores"),
			Aggregates.group("$scores.type", Accumulators.avg("mitjana", "$scores.score"))
		)).forEach((Document doc)-> {
			System.out.println("Tipus d'activitat: "+doc.getString("_id")+"  Nota mitjana: "+doc.getDouble("mitjana"));
		});
		client.close();
	}
}
