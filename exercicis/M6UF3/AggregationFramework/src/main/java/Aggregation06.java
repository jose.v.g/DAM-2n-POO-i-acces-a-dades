import java.util.Arrays;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;

public class Aggregation06 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("Practiques");
		MongoCollection<Document> coll = db.getCollection("grades");
		
		/*db.grades.aggregate([
		    {$unwind:"$scores"},
		    {$match:{"scores.type":"exam"}},
		    {$group:{_id:"$class_id",
		             mitjana:{$avg:"$scores.score"}}},
		    {$sort:{"mitjana":1}}
		])*/

		coll.aggregate(Arrays.asList(
				Aggregates.unwind("$scores"),
				Aggregates.match(Filters.eq("scores.type","exam")),
				Aggregates.group("$class_id", Accumulators.avg("mitjana", "$scores.score")),
				Aggregates.sort(Sorts.descending("mitjana"))
			)).forEach((Document doc) -> {
				System.out.println("Clase: " + doc.getInteger("_id")+", Mitjana d'examens: "+doc.getDouble("mitjana"));
			});
		client.close();
	}
}
