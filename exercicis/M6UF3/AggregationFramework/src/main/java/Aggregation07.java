import java.util.Arrays;
import java.util.Scanner;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;

public class Aggregation07 {

	public static void main(String[] args) {
		String codEstud;
		Scanner scan = new Scanner(System.in);
		
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("Practiques");
		MongoCollection<Document> coll = db.getCollection("grades");
		
		/*db.grades.aggregate([
        	{$unwind:"$scores"},
        	{$match:{"student_id":0}}
		])*/
		
		System.out.println("Introdueix el codi de l'estudiant a buscar: ");
		codEstud = scan.nextLine();

		coll.aggregate(Arrays.asList(
				Aggregates.unwind("$scores"),
				Aggregates.match(Filters.eq("student_id",Integer.parseInt(codEstud)))
			)).forEach((Document doc) -> {
				Document scores = doc.get("scores", Document.class);
				System.out.println("Alumno: " + doc.get("student_id")+", Activitat: "+scores.get("type")+" Puntuacio: "+scores.get("score"));
			});
		scan.close();
		client.close();
	}
}
