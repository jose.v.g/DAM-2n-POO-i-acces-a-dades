import java.util.Arrays;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Sorts;

public class Aggregation08 {

	String prevClass = "";
	
	public static void main(String[] args) {
		new Aggregation08().run();
	}
	
	public void run() {
		
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("Practiques");
		MongoCollection<Document> coll = db.getCollection("grades");
		
		/*db.grades.aggregate([
			{$unwind:"$class_id"},
			{$sort:{"class_id":1}}
		])*/

		coll.aggregate(Arrays.asList(
				Aggregates.unwind("$class_id"),
				Aggregates.sort(Sorts.ascending("class_id"))
			)).forEach((Document doc) -> {
				//doc.get("class_id").toString()
				if(!prevClass.equals(doc.get("class_id").toString())){
					System.out.println("Clase num."+doc.get("class_id").toString());
					prevClass = doc.get("class_id").toString();
				}
				System.out.println("  Alumno: " + doc.get("student_id"));
			});
		client.close();
	}
}
