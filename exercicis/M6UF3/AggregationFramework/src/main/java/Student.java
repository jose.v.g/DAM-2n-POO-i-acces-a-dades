public class Student {
	public String name;
	public int id;
	public double scoreQuiz;
	public double scoreHomework;
	public double scoreExam;
	
	public Student(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public double getFinalScore() {
		double finalScore = 0.1*getScoreQuiz() + 0.3*getScoreHomework() + 0.6*getScoreExam();
		if (getScoreExam()<40 && finalScore>40)
			finalScore=40;
		return finalScore;
	}
	
	public String toString() {
		String frFinal = "Alumne: "+name+" ("+id+") - Nota final: "+getFinalScore();
		return frFinal;
	}

	public double getScoreQuiz() {
		return scoreQuiz;
	}

	public void setScoreQuiz(double scoreQuiz) {
		this.scoreQuiz = scoreQuiz;
	}

	public double getScoreHomework() {
		return scoreHomework;
	}

	public void setScoreHomework(double scoreHomework) {
		this.scoreHomework = scoreHomework;
	}

	public double getScoreExam() {
		return scoreExam;
	}

	public void setScoreExam(double scoreExam) {
		this.scoreExam = scoreExam;
	}

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}
}