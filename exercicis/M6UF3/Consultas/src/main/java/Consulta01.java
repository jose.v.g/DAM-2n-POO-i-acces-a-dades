import static com.mongodb.client.model.Filters.all;
import static com.mongodb.client.model.Projections.include;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.json.JsonWriterSettings;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Consulta01 {

public static void main(String[] args) {
		
		String city;
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Introdueix poblaci�:");
		city = scan.nextLine();
		
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("Practiques");
	    MongoCollection<Document> coll = db.getCollection("zips");
	    JsonWriterSettings settings = new JsonWriterSettings(true);
		
	    List<Document> resultat;
	
		Bson filter = all("city", city.toUpperCase());
		Bson projection = include("_id");
		
		resultat = coll.find(filter).projection(projection).into(new ArrayList<Document>());
		
		for (Document doc : resultat){
			System.out.println(doc.toJson(settings));
		}
		client.close();
		scan.close();
	}
}
