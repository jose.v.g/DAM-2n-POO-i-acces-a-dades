import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.json.JsonWriterSettings;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;

public class Consulta02 {

	public static void main(String[] args) {
		/*Fes un programa que mostri els 10 codis postals amb m�s poblaci�.*/
		
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("Practiques");
	    MongoCollection<Document> coll = db.getCollection("zips");
	    JsonWriterSettings settings = new JsonWriterSettings(true);
	    
	    List<Document> resultat;
	    
	  
	    //java tradicional
	    Bson sort = Sorts.orderBy(Sorts.descending("pop"));
	    Bson projection = Projections.include("_id");
	    resultat = coll.find().projection(projection).sort(sort).limit(10).into(new ArrayList<Document>());
	    
	    for(Document doc : resultat){
	    	System.out.println(doc.toJson(settings));
	    }
	    
	    //java 8
	    //coll.find().projection(projection).sort(sort).limit(10).forEach((Document doc)  -> System.out.println(doc.toJson(settings)));
	    
	    client.close();

	}
}
