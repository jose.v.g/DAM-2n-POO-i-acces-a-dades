import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.json.JsonWriterSettings;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;

public class Consultes03 {

public static void main(String[] args) {
		
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("Practiques");
	    MongoCollection<Document> coll = db.getCollection("zips");
	    JsonWriterSettings settings = new JsonWriterSettings(true);

	    Bson sort = Sorts.orderBy(Sorts.ascending("loc.1"));
	    Bson projection = Projections.exclude("_id", "pop", "state");
	    
	    List <Document> resultat = coll.find().sort(sort).projection(projection).into(new ArrayList<Document>());
	    System.out.println(resultat.get(resultat.size()/2).toJson(settings));
	    
	    client.close();
	}
}
