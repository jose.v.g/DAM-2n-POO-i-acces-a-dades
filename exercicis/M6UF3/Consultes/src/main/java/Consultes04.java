import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.json.JsonWriterSettings;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;

public class Consultes04 {

	public static void main(String[] args) {

		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("Practiques");
		MongoCollection<Document> coll = db.getCollection("zips");
		JsonWriterSettings settings = new JsonWriterSettings(true); 

		Bson filter = Filters.lt("pop", 50);
		Bson sort = Sorts.ascending("city"); 
		List<Document> resultat = coll.find(filter).sort(sort).into(new ArrayList<Document>());

		for(Document doc : resultat){
			int repetit = 0;
			for (Document doc2 : resultat){
				if (doc.get("city").equals(doc2.get("city"))){
					repetit++;
				}
			}
			if(repetit == 1)
				System.out.println(doc.toJson(settings));	
		}
		client.close();
	}
}
