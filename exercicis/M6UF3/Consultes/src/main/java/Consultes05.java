import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;

public class Consultes05 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("Practiques");
		MongoCollection<Document> coll = db.getCollection("zips");
		
		List<String> states = coll.distinct("state", String.class).into(new ArrayList<String>());
		Collections.sort(states);
		for (String state : states) {
			Document first = coll.find(Filters.eq("state", state)).sort(Sorts.ascending("_id")).first();
			Document last = coll.find(Filters.eq("state", state)).sort(Sorts.descending("_id")).first();
			System.out.println("Estat "+state);
			System.out.println(" Codi postal m�s baix: "+first.get("_id").toString()+" ("+first.get("city").toString()+")");
			System.out.println(" Codi postal m�s alt: "+last.get("_id").toString()+" ("+last.get("city").toString()+")");
		}
		client.close();
	}
}
