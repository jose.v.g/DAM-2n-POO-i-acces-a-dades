import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;

public class Consultes06 {

	public static void main(String[] args) {
		int sum=0;
		
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("Practiques");
		MongoCollection<Document> coll = db.getCollection("zips");
		
		List<Document> zips = coll.find(Filters.eq("city", "Kansas City".toUpperCase())).sort(Sorts.ascending("_id")).into(new ArrayList<Document>());
		for (Document doc : zips) {
			System.out.println("Zip: "+doc.getInteger("_id")+" - Pop: "+doc.getInteger("pop"));
			sum+=doc.getInteger("pop");
		};
		System.out.println("Poblaci� total: "+sum);

		client.close();
	}
}
