import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;

public class Consultes07 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("Practiques");
		MongoCollection<Document> coll = db.getCollection("zips");
		
		List<Document> zips = coll.find(Filters.eq("city", "Springfield".toUpperCase())).sort(Sorts.ascending("state", "_id")).into(new ArrayList<Document>());
		System.out.println("Hi ha "+zips.size()+" poblacions anomenades Springfield.");
		Document lastZip = null;
		double lon=0, lat=0;
		int count=0;
		for (Document zip : zips) {
			if (lastZip==null || !lastZip.getString("state").equals(zip.getString("state"))) {
				if (lastZip!=null) {
					System.out.println("Població: "+lastZip.getString("city")+" - Localització: ("+lon/count+", "+lat/count+")");
				}
				System.out.println("Estat de "+zip.getString("state"));
				lat=lon=0;
				count=0;
			}
			@SuppressWarnings("unchecked")
			List<Double> latlon = (List<Double>)zip.get("loc");
			lat+=latlon.get(0);
			lon+=latlon.get(1);
			count++;
			lastZip = zip;
		}
		System.out.println("Població: "+lastZip.getString("city")+" - Localització: ("+lon/count+", "+lat/count+")");
		client.close();
	}
}
