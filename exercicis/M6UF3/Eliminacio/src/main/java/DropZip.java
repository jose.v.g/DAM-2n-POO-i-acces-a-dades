import static com.mongodb.client.model.Filters.all;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.json.JsonWriterSettings;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class DropZip {

	public static void main(String[] args) {
		
		String zip;
		String nPob;
		String resp = "";
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Introdueix zip:");
		zip = scan.nextLine();
		
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("Practiques");
	    MongoCollection<Document> coll = db.getCollection("zips");
	    JsonWriterSettings settings = new JsonWriterSettings(true);
		
	    List<Document> resultat;
	
		Bson filter = all("_id", zip.toUpperCase());
		
		resultat = coll.find(filter).into(new ArrayList<Document>());
		
		for (Document doc : resultat){
			System.out.println(doc.toJson(settings));
		}
		
		do{
			System.out.println("Vols modificar la poblacio?(Y/N)");
			resp = scan.nextLine();
			if(resp.equals("Y")){
				 System.out.println("Introdueix la nova poblacio: ");
				 nPob = scan.nextLine();
				 coll.updateOne(filter, new Document("$set",new Document("city",nPob)));
				 System.out.println("Poblacio modificada.");
				 break;
			 }else if(!resp.equals("N")){
				 System.out.println("Resposta no valida.");
			 }
		}while(resp != "N");
		
		client.close();
		scan.close();
	}
}
