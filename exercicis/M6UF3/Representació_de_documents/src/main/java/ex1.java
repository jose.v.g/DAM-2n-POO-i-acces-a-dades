import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.json.JsonWriterSettings;

public class ex1 {

	public static void main(String[] args) {
		ArrayList<Integer> id = new ArrayList<>();
		ArrayList<String> last_n = new ArrayList<>();
		ArrayList<String> first_n = new ArrayList<>();
		
		id.add(5);
		id.add(27);
		id.add(37);
		id.add(43);
		id.add(84);
		id.add(104);
		
		last_n.add("LOLLOBRIGIDA");
		last_n.add("MCQUEEN");
		last_n.add("BOLGER");
		last_n.add("JOVOVICH");
		last_n.add("PITT");
		last_n.add("CRONYN");
		
		first_n.add("JOHNNY");
		first_n.add("JULIA");
		first_n.add("VAL");
		first_n.add("KIRK");
		first_n.add("JAMES");
		first_n.add("PENELOPE");
		
		List<Document> llista = new ArrayList<>();
		Document documentLlista;
		for (int i = 0; i < id.size(); i++) {
			documentLlista = new Document().append("actorId", id.get(i))
										   .append("Last name", last_n.get(i))
										   .append("First name", first_n.get(i));
			llista.add(documentLlista);
		}
		
        Document document = new Document()
                .append("ID", 19)
                .append("Title", "AMADEUS HOLY")
                .append("Description","A Emotional Display of a Pioneer And a Technical Writer who must Battle a Man in A Baloon")
                .append("Rental Duration", 6)
                .append("Replacement Cost", 20.99)
                .append("Category", "Action")
                .append("Actors", llista);
                

        JsonWriterSettings settings = new JsonWriterSettings(true);
        System.out.println(document.toJson(settings));
    }
}
