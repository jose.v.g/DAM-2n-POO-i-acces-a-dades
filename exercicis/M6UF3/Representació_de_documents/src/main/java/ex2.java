import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.json.JsonWriterSettings;

public class ex2 {

	public static void main(String[] args) {
		ArrayList<Integer> ren_id = new ArrayList<>();
			ren_id.add(1185);
			ren_id.add(1476);
			ren_id.add(1725);
		ArrayList<String> ren_date = new ArrayList<>();
			ren_date.add("2005-06-15 00:54:12.0");
			ren_date.add("2005-06-15 21:08:46.0");
			ren_date.add("2005-06-16 15:18:57.0");
		ArrayList<String> ret_date = new ArrayList<>();
			ret_date.add("2005-06-23 02:42:12.0");
			ret_date.add("2005-06-25 02:26:46.0");
			ret_date.add("2005-06-17 21:05:57.0");
		ArrayList<Integer> staff_id = new ArrayList<>();
			staff_id.add(2);
			staff_id.add(1);
			staff_id.add(1);
		ArrayList<Integer> film_id = new ArrayList<>();
			film_id.add(611);
			film_id.add(308);
			film_id.add(159);
		ArrayList<String> f_title = new ArrayList<>();
			f_title.add("MUSKETEERS WAIT");
			f_title.add("FERRIS MOTHER");
			f_title.add("CLOSER BANG");
		
		ArrayList<Integer> pay_id = new ArrayList<>();
			pay_id.add(3);
			pay_id.add(5);
			pay_id.add(6);
		ArrayList<Double> amount = new ArrayList<>();
			amount.add(6.00);
			amount.add(10.00);
			amount.add(5.00);
		ArrayList<String> pay_date = new ArrayList<>();
			pay_date.add("2005-06-15 00:54:12.0");
			pay_date.add("2005-06-15 21:08:46.0");
			pay_date.add("2005-06-16 15:18:57.0");
		
		
		
		List<Document> llista = new ArrayList<>();
		Document documentLlista;
		for (int i = 0; i < ren_id.size(); i++) {
			documentLlista = new Document().append("rentalId", ren_id.get(i))
										   .append("Rental Date", ren_date.get(i))
										   .append("Return Date", ret_date.get(i))
										   .append("staffId", staff_id.get(i))
										   .append("filmId", film_id.get(i))
										   .append("Film Title", f_title.get(i))
										   .append("Payments", new Document().append("Payment Id", pay_id.get(i))
																	 		 .append("Amount", amount.get(i))
																     		 .append("Payment Date", pay_date.get(i)));
			llista.add(documentLlista);
		}
		
        Document document = new Document()
                .append("ID", 19)
                .append("First Name", "MARY")
                .append("Last Name","SMITH")
                .append("Adress", "1913 Hanoi Way")
                .append("District", "Nagasaki")
                .append("City", "Sasebo")
            	.append("Country", "Japan")
                .append("Phone", "28303384290")
                .append("Rentals", llista);
                

        JsonWriterSettings settings = new JsonWriterSettings(true);
        System.out.println(document.toJson(settings));
    }
}
